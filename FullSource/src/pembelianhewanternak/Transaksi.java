/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pembelianhewanternak;

/**
 *
 * @author Lenovo
 */
public class Transaksi extends Pembeli{
    private double hargaSatuan,bayar,banyak;

    public Transaksi(){}

    public Transaksi(String nama, String alamat, String noHp, double hargaSatuan, double bayar, double banyak) {
        super(nama, alamat, noHp);
        this.hargaSatuan = hargaSatuan;
        this.bayar = bayar;
        this.banyak = banyak;
    }

    public double getHargaSatuan() {
        return hargaSatuan;
    }

    public void setHargaSatuan(double hargaSatuan) {
        this.hargaSatuan = hargaSatuan;
    }

    public double getBayar() {
        return bayar;
    }

    public void setBayar(double bayar) {
        this.bayar = bayar;
    }

    public double getBanyak() {
        return banyak;
    }

    public void setBanyak(double banyak) {
        this.banyak = banyak;
    }

    public double Kembalian(double bayar, double setHarga, double banyak){
        setHarga*=banyak;
        return bayar-setHarga;
    }
    
    public double total(double banyak,double setHarga){
        return banyak*setHarga;
    }
}
