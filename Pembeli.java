package com.company;

public class Pembeli extends Manusia{
    private String alamat,noHp;

    public Pembeli(){

    }

    public Pembeli(String nama, String alamat, String noHp) {
        super(nama);
        this.alamat = alamat;
        this.noHp = noHp;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }
}
