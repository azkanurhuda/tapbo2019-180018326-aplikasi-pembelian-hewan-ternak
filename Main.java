package com.company;

import java.util.Scanner;

public class Main{

    public static void main(String[] args) {
        Pembeli pembeli = new Pembeli();
        Transaksi transaksi = new Transaksi();
        Scanner input = new Scanner(System.in);
        System.out.println("==========Data Pembeli=========");
        System.out.print("Masukan Nama = ");
        pembeli.setNama(input.nextLine());
        System.out.print("\nMasukan Alamat = ");
        pembeli.setAlamat(input.nextLine());
        System.out.print("\nMasukan No HP = ");
        pembeli.setNoHp(input.nextLine());

        System.out.println("\n==========Data Hewan==========");
        System.out.println("1. Sapi\n2. Kambing\n3. Ayam\n4. Bebek\n5. Kerbau");
        System.out.print("Masukan Pilihan = ");
        int pil = input.nextInt();
        switch (pil){
            case 1:
                transaksi.setNama("Sapi");
                break;
            case 2:
                transaksi.setNama("Kambing");
                break;
            case 3:
                transaksi.setNama("Ayam");
                break;
            case 4:
                transaksi.setNama("Bebek");
                break;
            case 5:
                transaksi.setNama("Kerbau");
                break;
            default:
                System.err.println("pilihan anda tidak ada");
                return;
        }

        System.out.print("Masukan Banyak hewan = ");
        transaksi.setBanyak(input.nextDouble());
        System.out.print("Masukan Harga Satuan = ");
        transaksi.setHargaSatuan(input.nextDouble());
        System.out.println("Total = " + transaksi.getHargaSatuan()*transaksi.getBanyak());
        System.out.print("Masukan Uang Bayar = ");
        transaksi.setBayar(input.nextDouble());
        System.out.println("\n");
        if ((transaksi.getHargaSatuan()*transaksi.getBanyak())<transaksi.getBayar()){
            System.out.println("Kembalian = " + transaksi.Kembalian(transaksi.getBayar(),transaksi.getHargaSatuan(),transaksi.getBanyak()));
            System.out.print("\nIngin Mencetak Struk, ketik 1? ");
            int y = input.nextInt();
            if (y==1){
                System.out.println("==========Struk Pembelian==========");
                System.out.println("Nama Pembeli : " + pembeli.getNama());
                System.out.println("Alamat Pembeli : "+ pembeli.getAlamat());
                System.out.println("No HP/Telepon Pembeli : "+ pembeli.getNoHp());
                System.out.println("Hewan Yang dibeli : "+transaksi.getNama());
                System.out.println("Jumlah Hewan yang dibeli : "+ transaksi.getBanyak());
                System.out.println("Harga Satuan "+transaksi.getNama()+" : "+transaksi.getHargaSatuan());
                System.out.println("Total Bayar : "+ (transaksi.getHargaSatuan()*transaksi.getBanyak()));
                System.out.println("Uang yang dibayarkan : "+ transaksi.getBayar());
                System.out.println("Kembalian Anda : "+ (transaksi.getBayar()-(transaksi.getHargaSatuan()*transaksi.getBanyak())));
                System.out.println("Terima kasih sudah belanja");
            } else {
                System.out.println("Terima kasih sudah berbelanja");
            }
        } else {
            System.out.println("Uang Anda Kurang");
        }
    }
}
